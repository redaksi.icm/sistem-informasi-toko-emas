function buatBaris()
{
	let no = $('#tabelTransaksi tbody tr').length + 1;
	let baris = `<tr>`;
	baris += `<td class="text-center">${no}</td>`;
	baris += `<td></td>`;
	baris += `<td></td>`;
	baris += `<td></td>`;
	baris += `<td></td>`;
	baris += `<td></td>`;
	baris += `<td></td>`;
	baris += `<td class="text-center"><button class="btn btn-danger" id="HapusBaris"><i class="fa fa-trash"></i></button></td>`;
	baris += `</tr>`;
	$('#tabelTransaksi tbody').append(baris);
}

function HitungTotalBayar()
{
	let Total = 0;
	$('#tabelTransaksi tbody tr').each(function(){
		if($(this).find('td:nth-child(8) input').val() > 0)
		{
			var SubTotal = $(this).find('td:nth-child(8) input').val();
			Total = parseInt(Total) + parseInt(SubTotal);
		}
	});

	$('#TotalBayar').html(to_rupiah(Total));
	$('#TotalBayarHidden').val(Total);

	$('#UangCash').val('');
	$('#UangKembali').val('');
}
