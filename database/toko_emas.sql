-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 10, 2020 at 03:01 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_emas`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `role` enum('Supervisor','Operator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `username`, `password`, `role`) VALUES
(1, '742', '113', 'Supervisor'),
(2, '4', '4', 'Operator'),
(3, 'dimza', 'dimza', 'Supervisor');

-- --------------------------------------------------------

--
-- Table structure for table `detail_trans_pembelian`
--

CREATE TABLE `detail_trans_pembelian` (
  `id_detail_pemb` int(11) NOT NULL,
  `id_trans_beli` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jml` float NOT NULL,
  `harga` int(11) NOT NULL,
  `subTotal` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_perhiasan`
--

CREATE TABLE `group_perhiasan` (
  `id_group` int(11) NOT NULL,
  `nama_group` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_perhiasan`
--

INSERT INTO `group_perhiasan` (`id_group`, `nama_group`) VALUES
(2, 'Tempa Perhiasan'),
(3, 'Emas');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_perhiasan`
--

CREATE TABLE `jenis_perhiasan` (
  `id_jenis_perhiasan` int(11) NOT NULL,
  `nama_jenis_perhiasan` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_perhiasan`
--

INSERT INTO `jenis_perhiasan` (`id_jenis_perhiasan`, `nama_jenis_perhiasan`) VALUES
(1, 'Cin - Cin'),
(2, 'Kalung'),
(3, 'Gelang');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id_supplier` int(11) NOT NULL,
  `nama` varchar(70) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(80) NOT NULL,
  `telp` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id_supplier`, `nama`, `alamat`, `kota`, `telp`) VALUES
(1, 'Asih Lama', 'Tasik', 'Tasik', '098999'),
(2, 'Bintang Jaya', 'Banjar', 'Banjar', '08999'),
(3, 'Mini', 'Tasik', 'Tasik', '08999'),
(4, 'Cepuk', 'entah', 'entah', '0899'),
(5, 'Dian Anyar', 'pwk', 'pwk', '0899');

-- --------------------------------------------------------

--
-- Table structure for table `trans_pembelian`
--

CREATE TABLE `trans_pembelian` (
  `id_trans_beli` int(11) NOT NULL,
  `no_nota` varchar(60) NOT NULL,
  `no_faktur` varchar(60) NOT NULL,
  `tanggal` date NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `grand_total` bigint(20) NOT NULL,
  `ket` text NOT NULL,
  `id_supplier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `detail_trans_pembelian`
--
ALTER TABLE `detail_trans_pembelian`
  ADD PRIMARY KEY (`id_detail_pemb`);

--
-- Indexes for table `group_perhiasan`
--
ALTER TABLE `group_perhiasan`
  ADD PRIMARY KEY (`id_group`),
  ADD KEY `id_group` (`id_group`);

--
-- Indexes for table `jenis_perhiasan`
--
ALTER TABLE `jenis_perhiasan`
  ADD PRIMARY KEY (`id_jenis_perhiasan`),
  ADD KEY `id_jenis_perhiasan` (`id_jenis_perhiasan`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id_supplier`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `trans_pembelian`
--
ALTER TABLE `trans_pembelian`
  ADD PRIMARY KEY (`id_trans_beli`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `detail_trans_pembelian`
--
ALTER TABLE `detail_trans_pembelian`
  MODIFY `id_detail_pemb` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_perhiasan`
--
ALTER TABLE `group_perhiasan`
  MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jenis_perhiasan`
--
ALTER TABLE `jenis_perhiasan`
  MODIFY `id_jenis_perhiasan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trans_pembelian`
--
ALTER TABLE `trans_pembelian`
  MODIFY `id_trans_beli` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
