<link rel="stylesheet" href="<?= base_url('assets/template') ?>/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('assets/template') ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<!-- <div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div>
				<div class="col-sm-6">

				</div>
			</div>
		</div>/.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-sm-3">

				<div class="card">
					<div class="card-header bg-primary">
						<h3 class="card-title"><b><i class="fa fa-file mr-1"></i> Informasi Nota</b></h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="form-horizontal">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-4 col-form-label">No Nota</label>
								<div class="col-sm-8">
									<input type="text" name="noNota" class="form-control" id="noNota" value="<?= $no_nota ?>" disabled>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-4 col-form-label">No Faktur</label>
								<div class="col-sm-8">
									<input type="text" name="noFaktur" class="form-control" id="noFaktur">
								</div>
							</div>							
						</div>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->

				<div class="card">
					<div class="card-header bg-primary">
						<h3 class="card-title"><b><i class="fa fa-user mr-1"></i> Informasi Supplier</b></h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="form-horizontal">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-4 col-form-label">Supplier</label>
								<div class="col-sm-8">
									<select name="id_supplier" id="id_supplier" class="form-control select2" style="width: 100%;">
										<option value="">--Pilih Supplier --</option>
										<?php foreach($supplier as $data): ?>
											<option value="<?= $data['id_supplier'] ?>"><?= $data['nama'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-4 col-form-label">No Telp</label>
								<div class="col-sm-8">
									<input type="text" name="telp" class="form-control" id="telp" disabled>
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-4 col-form-label">Alamat</label>
								<div class="col-sm-8">
									<textarea name="alamat" id="alamat" cols="30" rows="4" class="form-control" disabled></textarea>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->

			<div class="col-sm-9">
				<div class="card">
					<div class="card-header d-flex justify-content-end">						
						<a href="" onClick='window.location.reload()'>
								<i class="fa fa-sync"></i> Refresh Halaman
							</a>
						
					</div>
					<!-- /.card-header -->
					<div class="card-body">					
						<table class="table table-bordered mb-4" id="tabelTransaksi">
							<thead>
								<tr>
									<th>#</th>
									<th>Kode Barang</th>
									<th>Nama</th>
									<th>Kategori</th>
									<th>Harga Beli</th>
									<th>Qty</th>
									<th>Sub Total</th>
									<th></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div class='alert alert-success mb-4' id="TotalBayar">
							<div class="row">
								<div class="col-sm-3">
									<button id='BarisBaru' class='btn btn-default'><i class='fa fa-plus fa-fw'></i> Baris Baru (F7)</button>
								</div>
								<div class="col-sm-9 d-flex justify-content-end">
									<h2>Total : <span id='TotalBayar'>Rp. 0</span></h2>
									<input type="hidden" id='TotalBayarHidden'>
								</div>											
							</div>							
						</div>
						<div class="row">
							<div class="col-sm-7">
								<textarea name='catatan' id='catatan' class='form-control' rows='2' placeholder="Catatan Transaksi (Jika Ada)" style='resize: vertical; width:83%;'></textarea>
							</div>
							<div class="col-sm-5">
								<div class="form-horizontal">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">Bayar</label>
										<div class="col-sm-8">
										<input type="text" name="bayar" class="form-control" id="bayar">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-4 col-form-label">Tungggakan</label>
										<div class="col-sm-8">
										<input type="text" name="tunggakan" class="form-control" id="tunggakan">
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4"></div>
										<div class="col-sm-8">
											<button type='button' class='btn btn-primary btn-block' id='Simpan'>
												<i class='fa fa-save'></i> Simpan (F10)
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
		</div>
		<!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header bg-primary">
						<h3 class="card-title"><b><i class="fa fa-table mr-1"></i> History Pembelian</b></h3>
					</div>
					<div class="card-body">

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<?php $this->load->view('layouts/footer') ?>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
	<!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form action="#" id="data-formulir">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Data Supplier</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
					<button class="btn btn-primary" id="simpan-data">Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script src="<?= base_url('assets/template') ?>/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="<?= base_url('assets/template') ?>/dist/js/adminlte.min.js"></script>
<script src="<?= base_url('assets/template') ?>/dist/js/demo.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url('assets/template') ?>/plugins/select2/js/select2.full.min.js"></script>
<script>
	$(document).ready(function () {

		$('.select2').select2({theme: 'bootstrap4'});

		// $(document).on('keydown', 'body', function (e) {		
		// 	var charCode = ( e.which ) ? e.which : event.keyCode;
		// 	if(charCode === 113) //F2
		// 	{
		// 		$('#id_supplier').select2({theme: 'bootstrap4'});
		// 		$("#id_supplier").val('2').trigger('change');
		// 	}
		// });

		$("#id_supplier").on('change', function () {
			id_supplier = $(this).val();
			if (id_supplier) {
				getDataSupplier(id_supplier);
			}else{
				$("#telp").val('');
				$("#alamat").val('');
			}
		});
		
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 3000
		});

		function getDataSupplier(id)
		{
			$.ajax({
				type: "get",
				url: "<?= base_url('supplier/edit/') ?>"+id,
				dataType: "json",
				success: function (response) {
					$("#telp").val(response.telp);
					$("#alamat").val(response.alamat);
				}
			});
		}			
	});
</script>
</body>

</html>
