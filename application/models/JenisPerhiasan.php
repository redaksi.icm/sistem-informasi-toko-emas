<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class JenisPerhiasan extends CI_Model {	

	public function getDataTable()
	{
		$this->datatables->select('id_jenis_perhiasan, nama_jenis_perhiasan');
		$this->datatables->from('jenis_perhiasan');		
		$this->datatables->add_column('action', '<a href="#" class="btn btn-info" data-id="$1" data-toggle="modal" data-target="#modelId" id="buton_edit"><i class="fa fa-edit"></i> Edit</a> <a href="#" class="btn btn-danger" data-id="$1" id="buton_hapus"><i class="fa fa-trash"></i> Hapus</a>', 'id_jenis_perhiasan');
		return $this->datatables->generate();
	}

	public function getJenisPerhiasan($id = null)
	{
		if($id)
		{
			$this->db->select('nama_jenis_perhiasan');
			return $this->db->get_where('jenis_perhiasan', ['id_jenis_perhiasan' => $id])->row_array();			
		}else{
			return $this->db->get('jenis_perhiasan')->result_array();			
		}
	}

	public function simpanJenisPerhiasan($data)
	{
		$this->db->insert('jenis_perhiasan', $data);		
	}

	public function updateJenisPerhiasan($id, $data)
	{
		$this->db->update('jenis_perhiasan', $data, ['id_jenis_perhiasan' => $id]);		
	}

	public function deleteJenisPerhiasan($id)
	{
		$this->db->delete('jenis_perhiasan', ['id_jenis_perhiasan' => $id]);		
	}

}

/* End of file Supplier.php */
