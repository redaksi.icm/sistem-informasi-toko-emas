<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Model {

	public function getDataTable()
	{
		$this->datatables->select('id_group, nama_group');
		$this->datatables->from('group_perhiasan');		
		$this->datatables->add_column('action', '<a href="#" class="btn btn-info" data-id="$1" data-toggle="modal" data-target="#modelId" id="buton_edit"><i class="fa fa-edit"></i> Edit</a> <a href="#" class="btn btn-danger" data-id="$1" id="buton_hapus"><i class="fa fa-trash"></i> Hapus</a>', 'id_group');
		return $this->datatables->generate();
	}

	public function getGroup($id = null)
	{
		if($id)
		{
			$this->db->select('nama_group');
			return $this->db->get_where('group_perhiasan', ['id_group' => $id])->row_array();			
		}else{
			return $this->db->get('group_perhiasan')->result_array();			
		}
	}

	public function simpanGroup($data)
	{
		$this->db->insert('group_perhiasan', $data);		
	}

	public function updateGroup($id, $data)
	{
		$this->db->update('group_perhiasan', $data, ['id_group' => $id]);		
	}

	public function deleteGroup($id)
	{
		$this->db->delete('group_perhiasan', ['id_group' => $id]);		
	}	

}
