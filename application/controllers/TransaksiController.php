<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiController extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Datatables');
		$this->load->model('Supplier');
		$this->load->model('Transaksi');
	}
	

	public function indexPembelian()
	{
		$data['title'] = 'Transaksi Pembelian Barang';
		$data['layout'] = 'transaksi/transaksi_pembelian_view';
		$data['active'] = 'transaksi_pembelian';
		$data['supplier'] = $this->Supplier->getSupplier();
		$data['no_nota'] = $this->Transaksi->buatNota();
		$this->load->view('template', $data);
	}

	public function indexPenjualan()
	{
		$data['title'] = 'Transaksi Penjualan Barang';
		$data['layout'] = 'transaksi/transaksi_pembelian_view';
		$data['active'] = 'transaksi_penjualan';
		$data['supplier'] = $this->Supplier->getSupplier();
		$data['no_nota'] = $this->Transaksi->buatNota();
		$this->load->view('template', $data);
	}

}

/* End of file TransaksiController.php */
