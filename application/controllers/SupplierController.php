<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SupplierController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}
		$this->load->model('Supplier');
		$this->load->library('Datatables');		
	}
	

	public function index()
	{
		$data['title'] = 'Data Master Supllier';
		$data['layout'] = 'supplier/supplier_view';
		$data['active'] = 'supplier';
		$this->load->view('template', $data);
	}

	public function getDataTable()
	{
		header('Content-Type: application/json');
		echo $this->Supplier->getDataTable();
	}

	public function editSupplier($id)
	{
		$data = $this->Supplier->getSupplier($id);
		echo json_encode($data);
	}

	public function simpanSupllier()
	{
		$data = [
			'nama' => $this->input->post('nama_supplier'),
			'alamat' => $this->input->post('alamat_supplier'),
			'telp' => $this->input->post('no_telp_supplier'),
			'kota' => $this->input->post('kota_supplier'),			
		];
	
		$this->Supplier->simpanSupplier($data);

		echo json_encode(['OK']);
	}

	public function updateSupllier()
	{
		
		$id = $this->input->input_stream('id_supplier');
		
		$data = [
			'nama' => $this->input->input_stream('nama_supplier'),
			'alamat' => $this->input->input_stream('alamat_supplier'),
			'telp' => $this->input->input_stream('no_telp_supplier'),
			'kota' => $this->input->input_stream('kota_supplier'),			
		];		

		$this->Supplier->updateSupplier($id, $data);
		echo json_encode(['OK']);
	}

	public function deleteSupllier($id)
	{
		$this->Supplier->deleteSupplier($id);
		echo json_encode(['OK']);
	}

}
