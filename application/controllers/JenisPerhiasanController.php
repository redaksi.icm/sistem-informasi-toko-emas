<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class JenisPerhiasanController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('role') != 'Supervisor')
		{
			$this->session->set_flashdata('notif', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Forbidden</div>");
			redirect(base_url());
		}
		$this->load->model('JenisPerhiasan');
		$this->load->library('Datatables');
	}
	
	public function index()
	{
		$data['title'] = 'Data Master Jenis Perhiasan';
		$data['layout'] = 'jenis_perhiasan/jenis_perhiasan_view';
		$data['active'] = 'jenis_perhiasan';
		$this->load->view('template', $data);
	}

	public function getDataTable()
	{
		header('Content-Type: application/json');
		echo $this->JenisPerhiasan->getDataTable();
	}

	public function editJenisPerhiasan($id)
	{
		$data = $this->JenisPerhiasan->getJenisPerhiasan($id);
		echo json_encode($data);
	}

	public function simpanJenisPerhiasan()
	{
		$data = [
			'nama_jenis_perhiasan' => $this->input->post('nama')
		];
	
		$this->JenisPerhiasan->simpanJenisPerhiasan($data);

		echo json_encode(['OK']);
	}

	public function updateJenisPerhiasan()
	{		
		$id = $this->input->input_stream('id_jenis_perhiasan');
		
		$data = [
			'nama_jenis_perhiasan' => $this->input->input_stream('nama')
		];		

		$this->JenisPerhiasan->updateJenisPerhiasan($id, $data);
		echo json_encode([$this->input->input_stream()]);
	}

	public function deleteJenisPerhiasan($id)
	{
		$this->JenisPerhiasan->deleteJenisPerhiasan($id);
		echo json_encode(['OK']);
	}

}
